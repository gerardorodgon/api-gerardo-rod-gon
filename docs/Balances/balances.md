# Balances in Accounts and Cards

## Types of balances in Accounts and Cards

### Account and Card balances

An account have several main balances to show its position, which are related to the type of the account:

   * All account types are provided with these balances:
      + **Effective available balance:** this balance provides the effective amount that can be used for performing operations. That means the balance is updated in real time when an expense is performed, so this balance is the most useful balance regarding to perform operations.
         * For _debit cards_, this balance is the _Effective available balance_ of the account to which the operations performed with the card are debited, so it's not a real balance of the card, but does meet the idea of available money for that card.
      + **Account available balance:** this balance provides the account amount that can be used for perform operations. This balance differs from _Effective available balance_ in the way it's updated: when an operation is performed, _Effective available balance_ is immediately updated, while _Account available balance_ is updated once the operation has been accounted, which can happen some days after the operation was done.
         * For _debit cards_, this balance is the _Account available balance_ of the account to which the operations performed with the card are debited, so it's not a real balance of the card, but does meet the idea of account available money for that card.
		 * For _prepaid cards_, this balance is not provided since there's no accounting process for that kind of card.
   * Credit accounts are also provided with the following balances and amounts:
      + **Granted credit:** this amount provides the granted amount for this account/card. This is the maximum loaned amount by the bank to this account/card that can be used to perform operations. This amount remains unchanged althought operations are performed, apart from explicit renegotiations.
      + **Effective disposed balance:** this balance provides the effective amount that has been used from the granted credit. That means the balance is updated in real time when an expense is performed.
      + **Account disposed balance:** this balance provides the account amount that has been used from the granted credit. This balance differs from _Effective disposed balance_ in the way it's updated: when an operation is performed, _Effective disposed balance_ is immediately updated, while _Account disposed balance_ is updated once the operation has been accounted, which can happen some days after the operation was done.
   
### Credit Accounts/Cards: Relation between their balances

As explained above, credit accounts and cards have some additional balances regarding non-credit cards and accounts. The relation between their balances are explained with the following example of a credit card, but it's valid also for credit accounts:

   * **Day 1**: A customer hires a new credit card with 3.000€ as granted credit.
      + At this moment, the balances are:
         * _Granted credit_ -> 3.000€
         * _Effective available balance_ -> 3.000€
         * _Account available balance_ -> 3.000€
         * _Effective disposed balance_ -> 0€
         * _Account disposed balance_ -> 0€
      + The customer withdraws 150€, so the balances change as shown below:
         * _Granted credit_ -> 3.000€
         * _Effective available balance_ -> **2.850€**
         * _Account available balance_ -> 3.000€
         * _Effective disposed balance_ -> **150€**
         * _Account disposed balance_ -> 0€	
   * **Day 2**: Let's assume the accounting process is D+1
      + At the beginning of the day, the balances are:
         * _Granted credit_ -> 3.000€
         * _Effective available balance_ -> 2.850€
         * _Account available balance_ -> **2.850€**
         * _Effective disposed balance_ -> 150€
         * _Account disposed balance_ -> **150€**
      + The customer now perfoms a new purchase, spending 100€. The balances are modified this way:
         * _Granted credit_ -> 3.000€
         * _Effective available balance_ -> **2.750€**
         * _Account available balance_ -> 2.850€
         * _Effective disposed balance_ -> **250€**
         * _Account disposed balance_ -> 150€
      + After that, the customer spends 50€ in another purchase:
         * _Granted credit_ -> 3.000€
         * _Effective available balance_ -> **2.700€**
         * _Account available balance_ -> 2.850€
         * _Effective disposed balance_ -> **300€**
         * _Account disposed balance_ -> 150€	  
   * **Day 3**:
      + The day changes, the accounting process is executed so the operations are accounted and the account balances are updated:
         * _Granted credit_ -> 3.000€
         * _Effective available balance_ -> 2.700€
         * _Account available balance_ -> **2.700€**
         * _Effective disposed balance_ -> 300€
         * _Account disposed balance_ -> **300€**
	  
This is the behavior we can work out from the example above:

   * _Granted credit_ does not change
   * _Effective available balance_ decreases instantly with every operation
   * _Account available balance_ decreases with the cummulative amount of the operations done the day before when the accounting process is executed
   * _Effective disposed balance_ increases instantly with every operation
   * _Account disposed balance_ increases with the cummulative amount of the operations done the day before when the accounting process is executed
	  
Taking a deeper look to the example, we can infer the following formulas:

   * _Effective available balance_ + _Effective disposed balance_ = _Granted credit_
   * _Account available balance_ + _Account disposed balance_ = _Granted credit_

## How these balances are provided within APIs

Both accounts and cards can have the same balances and so the way they are designed within Accounts and Cards APIs is the same. Based on the terms used above, here is the correlation with APIs attributes:

   * **availableBalance**: since accounts and cards have two different available balances, effective and account, these balances are provided within a structure named _availableBalance_. This structure encloses both available balances besides another one that provides the difference between them:
      + **currentBalances**: this balance matches _Effective available balance_
      + **postedBalances**: this balance matches _Account available balance_
      + **pendingBalances**: this balance shows the diferencia between _Effective available balance_ and _Account available balance_ (availableBalance.currentBalances - availableBalance.postedBalances)   
   * **Granted credit**: this attribute is not really a balance since it does not change but it's helpful for understanding the _disposed balances_. Granted credit is provided throught the attribute **grantedCredits**.
   * **disposedBalance**: as in _available balance_, there are two different account balances, effective and account, and they're also enclosed under the same structure named _disposedBalance_. Apart from the mentioned balances, this structure provides and additional balance to show the difference between them:
      + **currentBalances**: this balance matches _Effective disposed balance_
      + **postedBalances**: this balance matches _Account disposed balance_
      + **pendingBalances**: this balance provides the difference between _Effective disposed balance_ and _Account disposed balance_ (disposedBalance.currentBalances - disposedBalance.postedBalances)
   
## Why balances are arrays

The attributes that provide the balances are named in plurar and that's because they are arrays. But, does it make any sense to provied the value of a balance in an array? This behavior is due to the following two reasons:

   * **Allow providing the local currency exchange** when the account/card is denominated in a foreign currency. Related to _currencies_ attribute which provides the list of currencies in which account/card amounts may be provided, there's a boolean attribute named _isMajor_ that points the denomination currency of the account/card when its value is true.
   * **Allow Chilean credit cards to fit with the global definition** since Chilean credit cards have two independent granted credits within the same credit card:
      + A granted credit in Chilean Pesos (CLP) for operations done in the local currency
      + Another granted credit in US Dollars (USD) for operations done in any foreign currency.