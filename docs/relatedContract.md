# Breaking the structure of related contracts

##What a related contract is

A related contract is *a resource that model an existing relation between two hired products* apart from sharing a participant.

As an example, let's say a customer has hired the following products:

* An account *ACCOUNT_1*
* A debit card *DEBIT_CARD_1* which purchases are debited to *ACCOUNT_1*
* A mortgage *MORTGAGE_1* which monthly instalments are direct debited to *ACCOUNT_1*

There's an implicit relation between these three products: they belong to the same customer. However, in terms of related contracts, the existing relations are:

* A relation *REL_1* between *ACCOUNT_1* and *DEBIT_CARD_1*
* A relation *REL_2* between *ACCOUNT_1* and *MORTGAGE_1*

There's no explicit relation between *DEBIT_CARD_1* and *MORTGAGE_1*, so there's no related contract to link them. The following schema shows these relations:

![Figure 1 - Relations Example](related_contracts_1.png =300x225)

Althought related contracts are resources, they are usually managed as subresources of any of the hired products involved. Based on the example above, the valid URLs would be like below:

* List of related contracts
```
GET /accounts/ACCOUNT_1/related-contracts
GET /cards/DEBIT_CARD_1/related-contracts
GET /loans/MORTGAGE_1/related-contracts
```

* Details of a specific related contract
```
GET /accounts/ACCOUNT_1/related-contracts/REL_1
GET /accounts/ACCOUNT_1/related-contracts/REL_2
GET /cards/DEBIT_CARD_1/related-contracts/REL_1
GET /loans/MORTGAGE_1/related-contracts/REL_2
```

## What a related contract is not
### A related contract is not a contract
Althought a related contract matches with a unique contract in most cases, looking into cards we can easily figure out why a related contract is not a contract, since each card-id is an independent card while two different card-ids can be hired under the same contract document. The following example enlights this statement:

* A card A and its renewal B match with two different card-id and there's an explicit relation between them: card A is the ancestor of card B (and so, card B is the successor of card A).
* On another hand, a contract can have two different credit cards: one for the customer and the other for his/her couple, so both cards share the granted credit to that contract.

#### If it's not a contract, why is it called related contract?
Related contract structure was named this way because of an existing structure in the Data Canonical Model that partially met this functionality. Data Canonical Model is a document used by several BBVA architectures to unify concepts and nomenclature prior to the creation of API Catalog.

### A related contract is not a physical nor a digital document signed by the customer
Related contracts do not refer to legal nor contractual documents related to the queried product. They do not refer to the legal agreements physically/digitally signed by the customer when hiring the product and they do not provide additional legal information about the hired product.

### A related contract is not an external product the customer uses
Related contracts are not intended to model any relation between BBVA products and non-BBVA products. For these kind of relations, there must be an operation to relate both products:

* If the monthly instalments of my mortgage A from another bank are directly debt against my BBVA account B, there must be an operation, i.e. direct-debts, to model this kind of operations where BBVA account B is the source of the money and external mortgage A is the mortgage to be paid.
* If there's a scheduled transfer from my BBVA account B to another person's account C, this relationship will not be modelled as a related contract but by an operation such scheduled transfer instead.

## What related contract structure is used for
Related contract structure is intended *to provide basic information about hired products the customer can operate with and which are explicitly related to the queried product*.

The base idea of the related contract structure is to easily know which other hired products are linked with a specific hired product. If more information about any of the related hired products is required, the best way is by invoking the method to get the details of the desired hired product.

## Understanding a related contract
### Usual structure and its meaning
Below it's shown pseudo-code structure about the common structure of a related contract response:
```
{
   relatedContractId
   contractId
   number
   numberType {
      id
	  name
   }
   alias
   productType {
      id
	  name
   }
   relationType {
      id
	  name
   }
}

```

where:

* _relatedContractId_: this is the identifier of the relationship. This id remains the same both when retrieving the related contracts from the product A and when retrieving them from the product B for a relationship between them.
* _contractId_: this is the main internal identifier of the related hired product. This id matches with account-id (when the related hired product is an Account), card-id (when the related hired product is a Card), loan-id (when the related hired product is a Loan), deposit-id (when the related hired product is a Deposit) and investment-fund-id (when the related hired product is an Investment Fund)
* _number_: this is the main reference number of the related hired product. This number is the public number for functional purposes i.e. IBAN number for a Spanish account or PAN for a card.
* _numberType_ (with an id-name structure): this attribute indicates the kind of number provided in number attribute i.e. the number is an IBAN number, a PAN number, a CLABE number...
* _alias_: custom name assigned by the customer to the related hired product.
* _productType_ (with an id-name structure): this attribute indicates the kind of product of the related hired product i.e. an account, a card, a loan...
* _relationType_ (with an id-name structure): this attribute indicates the kind of relationship with the related hired product from the point of view of the product provided in the URL. This relationType changes within a Related Contract depending on the product used to invoke related contracts service.

### But, if it's a relation between two products, why only information about one of them is shown?
Since Related Contracts are always managed as subresources of any of the hired products involved, a related contract is always queried from a preset point of view: the hired product used within the URL. This means a consumer knows one of the involved product in advance, so the consumer may already have its information or, at least, is able to retrieve its details by invoking the method that retrieves its detailed information.

### Why relation type changes depending on the point of view
The previous statement leads to a _custom_ relation type: the result will provide a self-explaining relation type about why the related product is related to the current one.

Let's think about relationship between Mike (father) and John (son):

* The relation (our related contract) means the same from both people's point of view
+ If we want to know Mike's relationships and their kind (our relation type), the result is:
      * John - Son
* If we want to know John's relationships and their kind, the result is:
      + Mike - Father

### A practical example
Let's take the example in the beginning of this document:

![Figure 1 - Relations Example](related_contracts_1.png =300x225)

Drilling into _List of related contracts_ invocations and applying the model defined for a related contract, these would be the results (JSON format):

* GET /accounts/ACCOUNT_1/related-contracts
```
[
   {
      "relatedContractId": “REL_1”,
      "contractId": “DEBIT_CARD_1”,
      "number": “4940 1111 2222 3333 4444”,
      "numberType": {
         "id": “PAN”,
         "name": “Permanent Account Number”
      },
      alias: “Shopping card”,
      "productType": {
         "id": “CARDS”,
         "name": “Cards”
      },
      "relationType": {
         "id": “DEBITED_PAYMENT_OPTION”,
         "name": Debited payment option”
      }
   },
   {
      "relatedContractId": “REL_2”,
      "contractId": “MORTGAGE_1”,
      "number": “0182-0000-11-222222222”,
      "numberType": {
	     "id": “CCC”,
		 "name": “Codigo Cuenta Cliente”
      },
      "alias": “Summer home mortgage”,
      "productType": {
	     "id": “LOANS”,
		 "name": “Loans”
	  },
      "relationType": {
	     "id": “DIRECT_DEBITED_LOAN”,
		 "name": “Direct debited loan"
	  }
   }
]
```
![Figure 2 - Getting ACCOUNT_1 Related Contracts example](related_contracts_2.png =300x225)

* GET /cards/DEBIT_CARD_1/related-contracts
```
[
   {
      "relatedContractId": “REL_1”,
      "contractId": “ACCOUNT_1”,
      "number": “ES90 0182 1642 0302 0153 7412”,
      "numberType": {
         "id": “IBAN”,
         "name": “International Banking Account Number”
      },
      "alias": “Main current account”,
      "productType": {
         "id": “ACCOUNTS”,
         "name": “Accounts”
      },
      "relationType": {
         "id": “MONEY_SOURCE”,
         "name": "Source of the money”
      }
   }
]
```
![Figure 3 - Getting DEBIT_CARD_1 Related Contracts example](related_contracts_3.png =300x225)

* GET /loans/MORTGAGE_1/related-contracts
```
[
   {
      "relatedContractId": “REL_2”,
      "contractId": “ACCOUNT_1”,
      "number": “ES90 0182 1642 0302 0153 7412”,
      "numberType": {
         "id": “IBAN”,
         "name": “International Banking Account Number”
      },
      alias: “Main current account”,
      "productType": {
         "id": “ACCOUNTS”,
         "name": “Accounts”
      },
      "relationType": {
         "id": “DIRECT_DEBIT_ACCOUNT”,
         "name": "Direct debit account”
      }
   }
]
```
![Figure 4 - Getting MORTGAGE_1 Related Contracts example](related_contracts_4.png =300x225)
